<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index()
    {
        $data = [
            'title' => 'User Data',
            'desc'  => 'Showing user data directly without RESTful API service.',
            'users' => $this->User_model->get_users(),
        ];
        $this->load->view('user_data_view', $data);
    }

}