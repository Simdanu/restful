<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MY_Controller {
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');

    }

    public function index()
    {
        $data = [
            'title'      => 'Registration',
            'server_url' => base_url()
        ];
        $this->load->view('registration_view', $data);
    }
}