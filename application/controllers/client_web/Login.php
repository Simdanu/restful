<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
    
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [
            'title'      => 'Login',
            'warning'    => '<b>UNDER CONSTRUCTION</b><br/> Just press Login if you want see the data!'
        ];
        $this->load->view('login_view', $data);
    }

}