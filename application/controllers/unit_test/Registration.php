<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MY_Controller {
    
    function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }

    function _remap($fn='',$ln=[]) {
        $this->index($fn,@$ln[0],@$ln[1],@$ln[2],@$ln[3],@$ln[4]);
    }

    public function index($fn='',$ln='',$dob='',$gen='',$em='',$mob='')
    {
        // Test for custome data from parameter, data must fill in url encode format
        if ($fn!=''&&$ln!=''&&$dob!=''&&$gen!=''&&$em!=''&&$mob!='') {
            $post_data = [
                'first_name' => urldecode($fn),
                'last_name'  => urldecode($ln),
                'dob'        => urldecode($dob),
                'gender'     => urldecode($gen),
                'email'      => urldecode($em),
                'mobile'     => urldecode($mob)
            ];
        } else {
            $post_data = [
                'first_name' => "Simon",
                'last_name'  => "Megadewandanu",
                'dob'        => "1989-11-26",
                'gender'     => "M",
                'email'      => "simon@simonndanu.info",
                'mobile'     => "+6281234567890"
            ];
        }
        $attribute = [
            'url'       => base_url('server_api/user/registration'),
            'method'    => 'POST',
            'post_data' => $post_data
        ];            
        $result = $this->curl_query($attribute);
        
        $name = 'The response result';
        $note = 'Response result must be array before send the result as json to client.';
        $this->unit->run($result, 'is_array', $name, $note);

        $name = 'The response result of status';
        $note = 'Response result always bring status response as boolean type. <br/>
                (PHP-Array can\'t show the bool value in some default browser)';
        $this->unit->run($result['status'], 'is_bool', $name, $note);

        echo 'Data-sent:  ';dd($post_data);
        echo 'Data-result:';dd($result);
        echo $this->unit->report();
    }
}