<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library("migration");
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function current()
	{
		if (!$this->migration->current()) {
			show_error($this->migration->error_string());
		} else {
			echo "Migrations current done.";
		}
	}

	public function latest()
	{
		if (!$this->migration->latest()) {
			show_error($this->migration->error_string());
		} else {
			echo "Migrations latest done.";
		}
	}

	public function version($version)
	{
		if (!$this->migration->version($version)) {
			show_error($this->migration->error_string());
		} else {
			echo "Migrations version $version done.";
		}
	}

}

?>