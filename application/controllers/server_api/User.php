<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        // Load the user model
        $this->load->model('User_model');
    }
    
    public function registration_post() {
        $first_name = strip_tags($this->post('first_name'));
        $last_name  = strip_tags($this->post('last_name'));
        $dob        = $this->post('dob');
        $gender     = $this->post('gender');
        $email      = strip_tags($this->post('email'));
        $mobile     = strip_tags($this->post('mobile'));

        $response['status'] = FALSE;

        if (empty($first_name))
            $response['error']['first_name'] = 'First Name is required'; 

        if (empty($last_name))
            $response['error']['last_name'] = 'Last Name is required'; 

        if (empty($email))
            $response['error']['email'] = 'Email is required'; 
        else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                $response['error']['email'] = 'Email is not a valid address';
            else {
                $id = $this->User_model->get_user(FALSE, [ 'email' => $email ]);
                if (!empty($id))
                    $response['error']['email'] = 'Email is already registered';
            }
        }

        if (empty($mobile))
            $response['error']['mobile'] = 'Mobile is required'; 
        else {
            if (!preg_match("/^(?:\+62)(?:\d(?:-)?){9,12}$/", $mobile))
                $response['error']['mobile'] = 'Mobile is not a valid Indonesian phone number';
            else {
                $id = $this->User_model->get_user(FALSE, [ 'mobile' => $mobile ]);
                if (!empty($id))
                    $response['error']['mobile'] = 'Mobile is already registered';
            }
        }
        
        //if (!empty($dob) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dob))
        if (!empty($dob) && !$this->validateDate($dob))
            $response['error']['dob'] = 'Date of Birth is not a valid date or format (YYYY-MM-DD)';

        $genders = [ 'M', 'F' ];
        if (!empty($gender) && !in_array($gender, $genders))
            $response['error']['gender'] = 'Gender is not valid (M/F)';

        if (!isset($response['error'])) {
            $data = [
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'dob'        => $dob,
                'gender'     => $gender,
                'email'      => $email,
                'mobile'     => $mobile
            ];
            $user_id = $this->User_model->set_user(false, $data);
            if ($user_id) {
                $response = [
                    'status'  => TRUE,
                    'user_id' => $user_id
                    ];
                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $response = [
                    'status'  => FALSE,
                    'message' => "Server can't create an user."
                    ];
                $this->response($response, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else 
            $this->response($response, REST_Controller::HTTP_OK);
    }

    private function validateDate($date, $format='Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public function ping_get(){
        $response = [
            'status'  =>  TRUE,
            'message' => "Ping Successfully!"
        ];
        $this->response($response, REST_Controller::HTTP_OK);
    }
}