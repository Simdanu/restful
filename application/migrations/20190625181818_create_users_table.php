<?php

class Migration_create_users_table extends CI_Migration
{
	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => true,
				),
			'first_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => 50,
				),
			'last_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => 50,
				),
			'dob' => array(
				'type' => 'DATE',
				'null' => true,
				),
			'gender' => array(
				'type' => 'ENUM("M","F")',
				'null' => true,
				),
			'email' => array(
				'type'       => 'VARCHAR',
				'constraint' => 100,
				),
			'mobile' => array(
				'type'       => 'VARCHAR',
				'constraint' => 20,
				),
			'created' => array(
				'type'       => 'INT',
				'constraint' => 10,
				'unsigned'   => true,
				),
			'updated' => array(
				'type'       => 'INT',
				'constraint' => 10,
				'unsigned'   => true,
				),
			'deleted' => array(
				'type'       => 'INT',
				'constraint' => 10,
				'unsigned'   => true,
				'null'       => true,
				'default'    => 0,
				),
			));
		$this->dbforge->add_key('id', true);
		
		$this->dbforge->create_table('users');
	}
	
	public function down()
	{
		$this->dbforge->drop_table('users');
	}
}

?>