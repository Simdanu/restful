<?php
/**
 * Check string var is json type
 * @param  String  $str
 * @return boolean
 */
function is_json($str) 
{
    if (is_string($str)) {
        @json_decode($str);
        return (json_last_error() === JSON_ERROR_NONE);
    }
    return false;
}

/**
 * Convert string to brcrypt text
 * @param  string $text
 * @return Bcrypt String
 */
function bcrypt($text='')
{
	return password_hash($text, PASSWORD_BCRYPT);
}

/**
 * Get full url with QUERY_STRING
 * @return string
 */
function current_full_url()
{
    $ci =& get_instance();

    $url = $ci->config->site_url($ci->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}

/**
 * Prints human-readable information about a variable
 * @param  Array/Object $var
 * @return print string
 */
function dd($var)
{
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}

/**
 * Convert 'one=1,two=2,three>3',four!=4 to array('one' => 1, 'two' => 2, 'three >' => 3, 'four !=' => 4)
 * @param  string $str
 * @return array
 */
function str2array($str)
{
    $array = array();
    $ex_str1 = explode(';', $str);
    foreach ($ex_str1 as $row) {
        if (strpos($row, '=') !== false) {
            $ex_str2 = explode('=', trim($row));
            $key = trim($ex_str2[0]);
            $val = explode(',', trim($ex_str2[1]));
        } elseif (strpos($row, '>') !== false) {
            $ex_str2 = explode('>', trim($row));
            $key = trim($ex_str2[0]).'>';
            $val = explode(',', trim($ex_str2[1]));
        } elseif (strpos($row, '>=') !== false) {
            $ex_str2 = explode('>=', trim($row));
            $key = trim($ex_str2[0]).'>=';
            $val = explode(',', trim($ex_str2[1]));
        } elseif (strpos($row, '<') !== false) {
            $ex_str2 = explode('<', trim($row));
            $key = trim($ex_str2[0]).'<';
            $val = explode(',', trim($ex_str2[1]));
        } elseif (strpos($row, '<=') !== false) {
            $ex_str2 = explode('<=', trim($row));
            $key = trim($ex_str2[0]).'<=';
            $val = explode(',', trim($ex_str2[1]));
        } elseif (strpos($row, '!=') !== false) {
            $ex_str2 = explode('!=', trim($row));
            $key = trim($ex_str2[0]).'!=';
            $val = explode(',', trim($ex_str2[1]));
        }
        $array[$key] = $val;
    }
    return $array;
}

/**
 * Input POST
 * @param  string $str
 * @return POST variable
 */
function ci_post($str)
{
    $ci =& get_instance();
    return $ci->input->post($str);
}

/**
 * Input GET
 * @param  string $str
 * @return GET variable
 */
function ci_get($str)
{
    $ci =& get_instance();
    return $ci->input->get($str);
}

/**
 * [clear_rn description]
 * @param  [type] $text [description]
 * @return [type]       [description]
 */
function clear_rn($text)
{
    return preg_replace("/[\r\n]{2,}/", " ", $text);
}

/**
 * XML object to Array
 * @param  SimpleXmlObject $xmlObject
 * @param  array  $out
 * @return array
 */
function xmlObj2array ($xmlObject, $out = array ())
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

    return $out;
}

/**
 * If Isset
 * @param variable
 */
function if_isset($value, $default='')
{
    if (isset($value))
        return $value;
    else
        return $default;
}