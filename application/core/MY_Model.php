<?php

class MY_Model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function db_get($id = false, $table, $id_name = 'id', $id2_name = '')
	{
		if ($id !== false)
		{
			if ((is_numeric($id) || $id2_name == '') && $id_name !== '')
			{
				$this->db->where($id_name, $id);
			} else {
				$this->db->where($id2_name, $id);
			}
		}

		$result = $this->db->get($table);
		return $result->row_array();
	}

	function db_gets($table)
	{
		$result = $this->db->get($table);
		if ($result === false)
			var_dump($this->db->queries);
		return $result->result_array();
	}

	function db_count($table)
	{
		return $this->db->count_all_results($table);
	}

	function db_where($where = array())
	{
		if (!empty($where))
		{
			foreach ($where as $key => $value)
			{
				if (is_array($value))
					$this->db->where_in($key, $value);
				else
					$this->db->where($key, $value);
			}
		}
	}

	function db_set($id, $data, $table, $id_name = 'id', $timestamp = true)
	{
		if ($timestamp)
			$data['updated'] = time();
		if (!empty($id))
		{
			$this->db->where($id_name, $id);
			$this->db->update($table, $data);
			return $id;
		} else {
			if ($timestamp && empty($data['created']))
				$data['created'] = time();
			$this->db->insert($table, $data);
			return $this->db->insert_id();
		}
	}

	function db_del($id = false, $table, $id_name = 'id', $id2_name = '')
	{
		if ($id !== false)
		{
			if (is_numeric($id) || $id2_name == '')
			{
				$this->db->where($id_name, $id);
			} else {
				$this->db->where($id2_name, $id);
			}
		}

		$this->db->delete($table);
	}
}

?>