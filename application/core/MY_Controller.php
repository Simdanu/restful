<?php

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
        parent::__construct();
        //date_default_timezone_set('Asia/Jakarta');
	}

    protected function curl_query($attribute = array()) {
        $param = '';
        if (isset($attribute['get_param'])) {       
            $param = http_build_query($attribute['get_param'], '', '&');
            $param = '?'.preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $param);
        }
        // generate the extra headers
        //$headers = array();
        static $ch = null;
        if (is_null($ch)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        }
        curl_setopt($ch, CURLOPT_URL, $attribute['url'].$param);
        if ($attribute['method']=='POST') {
            $post_data = http_build_query($attribute['post_data'], '', '&');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        // run the query
        $res = curl_exec($ch);
        if ($res === false) throw new Exception('Could not get reply:'.curl_error($ch));
        $dec = json_decode($res, true);
        
        if (!$dec) throw new Exception('Invalid data received, please make sure connection is working and requested API exists: '.$res);
        curl_close($ch);
        $ch = null;

        return $dec;
    }

	protected function newline()
    {
        if (PHP_SAPI === 'cli')
            return PHP_EOL;
        else
            return "<br/>";
    }
}

?>