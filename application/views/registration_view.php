<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<style type="text/css">
		html, body
		{
		    height: 90%;
		}
		.footer 
		{
		    position: fixed;
		    max-height: 100px;
		    height: 100px;
		    bottom: 0;
		    width: 100%;
		    background-color: #6200C0;
			padding-top: 15px;
			color: #fff;
		}
		.button-wrapper 
		{
			width: 70%;
		}
		.registration
		{
			max-width: 450px;
			margin-top: 5%;
		}
		.form-gender
		{
			margin-top: 10px;
			margin-bottom: 5px;
		}
		.form-gender .form-group
		{
			margin-right: 10px;
		}
		.register
		{
			background-color: #6200C0;
			color: #fff;
		}
		.login
		{
			background-color: #6200C0;
			color: #fff;
			width: 10px; 
		}
		.register:hover, .login:hover, .register:focus, .login:focus
		{
			background-color: #9040DC;
			color: #fff !important;
		}
		.popover
		{
		    color: white;
		}
		.error
		{
			width: 50%;
		}
		.disabledEvents
		{
		    pointer-events: none;
		    opacity: 0.4;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</head>
<body>
<div class="container">
	<div class="panel panel-default center-block registration">
        <div class="panel-heading">
        	<h3><?php echo $title; ?></h3>
        	<div class="btn-group pull-right error"></div>
        </div>
        <form id="registration_form">
	        <div class="panel-body">

	        	<div class="col-md-12 form-group">
					<input type="text" name="mobile" id="form_mobile" class="form-control" placeholder="Mobile Number">
				</div>
				<div class="col-md-12 form-group">
					<input type="text" name="first_name" id="form_first_name" class="form-control" placeholder="First Name">
				</div>
				<div class="col-md-12 form-group">
					<input type="text" name="last_name" id="form_last_name" class="form-control" placeholder="Last Name">
				</div>
				<div class="col-md-12 form-inline">
					<div> 
						<label>Date of Birth</label>
					</div>
					<div class="form-group">
						<select id="form_month" class="form-control">
				        	<option value="">Month</option>
				        	<option value="01">January</option>
				        	<option value="02">February</option>
				        	<option value="03">March</option>
				        	<option value="04">April</option>
				        	<option value="05">May</option>
				        	<option value="06">June</option>
				        	<option value="07">July</option>
				        	<option value="08">August</option>
				        	<option value="09">September</option>
				        	<option value="10">October</option>
				        	<option value="11">November</option>
				        	<option value="12">Desember</option>
				     	</select>
				    </div>
				    <div class="form-group">
				     	<select id="form_day" class="form-control">
				        	<option value="">Date</option>
				        	<?php for($x=1;$x<=28;$x++) { 
				        			$day = str_pad($x, 2, "0", STR_PAD_LEFT); ?>
				        		<option value="<?php echo $day?>"><?php echo $day?></option>
				     		<?php } ?>
				     	</select>
				    </div>
				    <div class="form-group">
				     	<select id="form_year" class="form-control">
				        	<option value="">Year</option>
				        	<?php for($x=1950;$x<=(int)date("Y");$x++) { ?>
				        		<option value="<?php echo $x?>"><?php echo $x?></option>
				     		<?php } ?>
				     	</select>
				    </div>
				</div>
				<div class="col-md-12 form-inline form-gender">
					<div class="form-group">
					    <input class="form-check-input" type="radio" name="gender" id="gender_m" value="M">
					    <label class="form-check-label">Male</label>
					</div>
					<div class="form-group">
					    <input class="form-check-input" type="radio" name="gender" id="gender_f" value="F">
					    <label class="form-check-label">Female</label>
					</div>
				</div>
				<div class="col-md-12 form-group">
					<input type="text" name="email" id="form_email" class="form-control" placeholder="Email">
				</div>
				<button type="submit" class="btn btn-block register">Register</button>
	        </div>
	    </form>
    </div>
	
</div>
<div class="center-block text-center footer">
	Footer
</div>
<script type="text/javascript">
	var registration = {
	    init: function() {
	        registration.register();
	    },
	    register: function() {
        	var request;
			// Bind to the submit event of our form
			$('#registration_form').submit(function(event){
			    // Prevent default posting of form - put here to work in case of errors
			    event.preventDefault();
			    // Abort any pending request
			    if (request) {
			        request.abort();
			    }
			    // setup some local variables
			    var $form = $(this);
			    // Let's select and cache all the fields
			    var $inputs = $form.find("input, radio");
			    // Serialize the data in the form
			    var serializedData = $form.serialize();
				if ($('#form_year').val()!='' || $('#form_month').val()!='' || $('#form_day').val()!='') {
				    dob = $('#form_year').val()+'-'+$('#form_month').val()+'-'+$('#form_day').val();
				    serializedData = serializedData+"&dob="+dob;
			    }
			    $inputs.prop("disabled", true);
			    console.log(serializedData);
			    request = $.ajax({
					url : "<?php echo $server_url; ?>server_api/user/registration",
					type: "post",
					data: serializedData
			    });
			    // Callback handler that will be called on success
			    request.done(function (response, textStatus, jqXHR){
			        if (response.error !== undefined) {
			        	$inputs.prop("disabled", false);
			        	var msg = '';
			        	$.each(response.error, function( k, v ) {
						  	//console.log( "Key: " + k + ", Value: " + v );
						  	msg = msg + "- " + v + "<br/>";
						});
					  	$('.error').attr("data-toggle", "popover");
					  	$('.error').attr("data-container", "body");
					  	$('.error').attr("data-placement", "top");
					  	$('.error').attr("data-html", "true");
						$('.error').attr("data-content", msg);
						$('[data-toggle="popover"]').popover('show');
			        } else {
			        	$('[data-toggle="popover"]').popover('destroy');
			    		$('.registration').addClass("disabledEvents");
			    		$('.footer').css("background-color", "transparent"); 
			    		$('.footer').empty().append('<div class="center-block button-wrapper"><button type="button" class="btn btn-block login">Login</button></div>');
			    	}
			    });

			    // Callback handler that will be called on failure
			    request.fail(function (jqXHR, textStatus, errorThrown){
			        // Log the error to the console
			        console.error(
			            "The following error occurred: " + textStatus, errorThrown
			        );
			    });

			    request.always(function () {
			        $inputs.prop("disabled", false);
			    });
			});

			$(document).on('click', '.login', function(e) {
				window.location.href = "<?php echo $server_url; ?>client_web/login";
			});

			$(document).on('change', '#form_month, #form_year', function(e) {
				var day   = $("#form_day").val();
				var month = $("#form_month").val();
				var year  = $("#form_year").val();
				if (month !== '') {
					$("#form_day").empty();
					$("#form_day").append('<option value="">Date</option>');
					for (i = 1; i <= 28; i++) { 
						var d = pad(i, 2);
						$("#form_day").append('<option value="'+d+'">'+d+'</option>');
					}
					if (month !== "02") {
						$("#form_day").append('<option value="29">29</option>');
						$("#form_day").append('<option value="30">30</option>');
					} else {
						if (year!=='' && year%4==0) {
							$("#form_day").append('<option value="29">29</option>');	
						}
					}
					var odd = ["01", "03", "05", "07", "08", "10", "12"];
					if($.inArray(month, odd) !== -1) {
						$("#form_day").append('<option value="31">31</option>');
					}
					$("#form_day").val(day);
				}
			});

			function pad (str, max) {
			 	str = str.toString();
			 	return str.length < max ? pad("0" + str, max) : str;
			}				
    	},
	};
	registration.init();	
</script>
</body>
</html>