<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<style type="text/css">
		html, body
		{
		    height: 90%;
		}
		.login
		{
			max-width: 450px;
			margin-top: 5%;
		}
		.log-in
		{
			background-color: #6200C0;
			color: #fff;
		}
		.log-in:hover, .log-in:focus
		{
			background-color: #9040DC;
			color: #fff !important;
		}
		.popover
		{
		    color: white;
		}
		.error
		{
			width: 50%;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</head>
<body>
<div class="container">
	<div class="panel panel-default center-block login">
        <div class="panel-heading">
        	<h3><?php echo $title; ?></h3>
        	<div class="btn-group pull-right error" data-container="body" data-toggle="popover" data-placement="top" data-html="true" data-content="<?php echo $warning; ?>"></div>
        </div>
        <form id="login_form">
	        <div class="panel-body">

	        	<div class="col-md-12 form-group">
					<input type="text" name="email" id="form_email" class="form-control" placeholder="Email">
				</div>
				<div class="col-md-12 form-group">
					<input type="password" name="password" id="form_password" class="form-control" placeholder="Password">
				</div>

				<button type="submit" class="btn btn-block log-in">Login</button>
	        </div>
	    </form>
    </div>
</div>
<script type="text/javascript">
	var login = {
	    init: function() {
	        login.log_in();
	    },
	    log_in: function() {
	    	$('[data-toggle="popover"]').popover('show');
        	$('#login_form').submit(function(event){
			    // Prevent default posting of form - put here to work in case of errors
			    event.preventDefault();
			    window.location.href = "<?php echo base_url('client_web/user'); ?>";
			});
    	},
	};
	login.init();	
</script>
</body>
</html>