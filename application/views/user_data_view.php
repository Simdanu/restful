<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<style type="text/css">
		html, body
		{
		    height: 90%;
		}
		.content-header
		{
			margin-bottom: 30px;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</head>
<body>
<div class="container">
	<div class="content-header center-block text-center">
		<h2><?php echo $title; ?></h2>
		<h4><?php echo $desc; ?></h4>
	</div>
	<div class="content-main">
		<table class="table table-hover">
			<tr>
				<th>ID</th>
				<th>Full Name</th>
				<th>Date of Birth</th>	
				<th>Gender</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Created</th>
			</tr>
			<?php foreach ($users as $user) { ?>
				<tr>
					<td><?php echo $user['id']; ?></td>
					<td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
					<td><?php if (!empty($user['dob'])) { echo date("d M Y", strtotime($user['dob'])); } ?></td>
					<td><?php if ($user['gender']=="M"){ echo "Male"; } if ($user['gender']=="F"){ echo "Female"; } ?></td>
					<td><?php echo $user['email']; ?></td>
					<td><?php echo $user['mobile']; ?></td>
					<td><?php echo date("Y-m-d h:i:s A",$user['created']); ?></td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>
</body>
</html>