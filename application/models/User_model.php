<?php

class User_model extends MY_Model
{
	var $table_name = "users";

	public function set_user($id, $data)
	{
		return $this->db_set($id, $data, $this->table_name, 'id');
	}

	public function get_user($id, $where=[])
	{		
		if (!empty($where))
			$this->db_where($where);

		return $this->db_get($id, $this->table_name, 'id');
	}

	public function get_users($where=array(), $attribute=array(), $count=false)
	{
		if (!empty($attribute['select']))
			$this->db->select($attribute['select']);
		if (!empty($where))
			$this->db_where($where);
		if (!empty($attribute['group_by']))
			$this->db->group_by($attribute['group_by']);
		if (!empty($attribute['order_by']))
			$this->db->order_by($attribute['order_by']);
		if (!empty($attribute['limit'])){
			$start = (!empty($attribute['start'])) ? $attribute['start'] : 0;
			$this->db->limit($attribute['limit'], $start);
		}

		if ($count) {
			return $this->db_count($this->table_name);
		} else {
			$result = $this->db_gets($this->table_name);
			if (!empty($attribute['total']))			
				return $result[0];
			else
				return $result;
		}
	}

	public function del_user($id)
	{
		return $this->db_del($id, $this->table_name);
	}
}

?>