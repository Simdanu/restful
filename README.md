# restful

The simple restful project with CodeIgniter framework both of server (API) and client (web). Including the unit test that can run on CLI.

**Description:**

There are 3 parts of this apps:
1. Server side (API)
2. Client side (Web)
3. Unit test (Web)

Idealy they should be separated into different application. But for simplying, I make them in the one apps which they all can be accommodated by PHP CodeIgniter framework.

**Technology**:
1. Server   : provide RESTful API, unit test - PHP-CodeIgniter
2. Web      : get RESTful - jQuery, PHP-CodeIgniter, HTML5, Bootstrap 3
3. Database : mySQL

**Instal:**

1. Set the database connection using database_example.php to make database.php - LOCATION: application/config/
2. Set the $config['base_url'] depend on your localhost domain name, default 'http://local.homes-report.me' - LOCATION: application/config/config.php
3. Create the database by executing [domain]/migrate/latest
4. Run the Web client - Registration Form by accessing main domain or [domain]/client_web/registration
5. Run the Unit Test - Registration Form by accessing [domain]/unit_test/registration, you can add the custom parameter by slash (/) in order
6. Enjoy!
